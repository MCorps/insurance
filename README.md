# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A FinLeap Case Study
* Version 0.1

### How do I get set up? ###

* Summary of set up
* The project can be cloned or downloaded from https://MaMoreo@bitbucket.org/MCorps/insurance.git

* Configuration: No further configuration is needed

* Dependencies: JUnit 4

* How to run tests
* The Test Suite "AllTests" is provided. Simply execute it as JUnit. 

* Deployment instructions
* Pipelines has been enabled for this product.

### Future Work ###
* Add Gradle to this product and connect it to Pipelines.
* Enhance the User Class: some fields/methods are missing
* Create the UI


### Who do I talk to? ###

* miguelangel.moreo@gmail.com