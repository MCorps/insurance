package org.finleap.model;

public class Electronics extends InsuranceProduct {

	private final Integer coverageLow = 500;
	private final Integer coverageHigh = 6000;
	private final Integer risk = 35;
	
	/**
	 * Fields: ID: Unique Id to identify this resource.
	 */
	
	@Override
	public int getCoverageLow(){
		return coverageLow;
	}
	
	@Override
	public int getCoverageHigh(){
		return coverageHigh;
	}

	@Override
	public int getRisk() {
		return risk;
	}
}
