package org.finleap.model;

public class SportsEquipment extends InsuranceProduct {

	private final Integer coverageLow = 0;
	private final Integer coverageHigh = 20000;
	private final Integer risk = 30;
	
	/**
	 * Fields: ID: Unique Id to identify this resource.
	 */
	
	@Override
	public int getCoverageLow(){
		return coverageLow;
	}
	
	@Override
	public int getCoverageHigh(){
		return coverageHigh;
	}

	@Override
	public int getRisk() {
		return risk;
	}
}
