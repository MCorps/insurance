package org.finleap.model;

import java.util.HashMap;
import java.util.Map;

public class User {

	/**
	 * User fields: name, password...
	 */

	// The list of products for this user.
	private Map<InsuranceProduct, Integer> insurancedProducts = new HashMap<InsuranceProduct, Integer>();

	// the total price to pay for this user.
	private int price = 0;

	/**
	 * Adds a product to be insuranced, if the coverage is in the range.
	 * 
	 * @param product
	 *            The product to cover
	 * @param coverage
	 *            the quantity to cover (it should belong to a range)
	 * 
	 * @return {@code true} if the product was successfully added, <br>
	 *         {@code false} otherwise
	 */
	public boolean addProduct(InsuranceProduct product, Integer coverage) {
		boolean checkCoverage = product.checkCoverage(coverage);
		if (checkCoverage) {
			insurancedProducts.put(product, coverage); // TODO: Check if exists?
		}
		return checkCoverage;
	}

	/**
	 * Calculates the price to pay for all the insuranced products.
	 * 
	 * @return
	 */
	public int calculatePrice() {
		for (Map.Entry<InsuranceProduct, Integer> entry : insurancedProducts.entrySet()) {
			InsuranceProduct product = entry.getKey(); // key = product
			Integer coverage = entry.getValue(); // value = user coverage

			price += product.calculatePrice(coverage);
		}
		return price;
	}

	/**
	 * Returns all the products insuranced by this user.
	 * 
	 * @return
	 */
	/*
	 * public Map<InsuranceProduct, Integer> getInsurancedProducts(){ return
	 * insurancedProducts; }
	 */

	/**
	 * Removes a product from this user..
	 * 
	 * @param product
	 *            The product to cover
	 */
	public void removeProduct(InsuranceProduct product) {
		if (insurancedProducts.containsKey(product)) {
			insurancedProducts.remove(product);
		}
	}

}
