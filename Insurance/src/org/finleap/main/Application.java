package org.finleap.main;

import org.finleap.model.Bike;
import org.finleap.model.Electronics;
import org.finleap.model.Jewelry;
import org.finleap.model.SportsEquipment;
import org.finleap.model.User;

/**
 * A simple Main to simulate the use. 
 *
 */
public class Application {

	public static void main(String[] args) {
		User testUser = new User();
		
		testUser.addProduct(new Bike(), 2000);
		testUser.addProduct(new Jewelry(), 10000);
		testUser.addProduct(new Electronics(), 1200);
		testUser.addProduct(new SportsEquipment(), 5000);
		
		System.out.println("Price to pay is " + testUser.calculatePrice());
	}

}
