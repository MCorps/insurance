/**
 * 
 */
package org.finleap.tests;

import static org.junit.Assert.*;

import org.finleap.model.Bike;
import org.junit.Before;
import org.junit.Test;

/**
 * This Class tests the Bike Class.
 *
 */
public class BikeTest {

	private Bike testBike;

	@Before
	public void init() {
		testBike = new Bike();
	}

	/**
	 * Test method for {@link org.finleap.model.Bike#getCoverageLow()}.
	 */
	@Test
	public void testGetCoverageLow() {
		assertEquals(0, testBike.getCoverageLow());
	}

	/**
	 * Test method for {@link org.finleap.model.Bike#getCoverageHigh()}.
	 */
	@Test
	public void testGetCoverageHigh() {
		assertEquals(3000, testBike.getCoverageHigh());
	}

	/**
	 * Test method for {@link org.finleap.model.Bike#getRisk()}.
	 */
	@Test
	public void testGetRisk() {
		assertEquals(30, testBike.getRisk());
	}

	/**
	 * Test method for
	 * {@link org.finleap.model.InsuranceProduct#calculatePrice(int)}.
	 */
	@Test
	public void testCalculatePrice() {
		assertEquals(900, testBike.calculatePrice(3000));
	}

	/**
	 * Test method for
	 * {@link org.finleap.model.InsuranceProduct#checkCoverage(java.lang.Integer)}
	 * .
	 */
	@Test
	public void testCheckCoverage() {
		assertTrue(testBike.checkCoverage(0));  // low coverage
		assertTrue(testBike.checkCoverage(3000)); //high coverage
		assertFalse(testBike.checkCoverage(4000)); //coverage is too high
	}

}
