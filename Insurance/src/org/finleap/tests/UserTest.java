package org.finleap.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.finleap.model.Bike;
import org.finleap.model.Jewelry;
import org.finleap.model.User;
import org.junit.Before;
import org.junit.Test;

public class UserTest {

	// The instance Object to Test
	private User testUser;

	@Before
	public void init() {
		testUser = new User();
	}

	@Test
	public void testAddProduct() {
		assertTrue(testUser.addProduct(new Bike(), 200));
	}

	@Test
	public void testAddProductWrongRange() {
		assertFalse(testUser.addProduct(new Bike(), 5000));
	}

	@Test
	public void testCalculatePrice() {
		testUser.addProduct(new Bike(), 2000);
		assertEquals(600, testUser.calculatePrice());
	}

	@Test
	public void testCalculatePriceSeveralProducts() {
		testUser.addProduct(new Bike(), 2000);
		testUser.addProduct(new Jewelry(), 10000);
		assertEquals(1100, testUser.calculatePrice());
	}

	// TODO: Add test for removeProduct
}
